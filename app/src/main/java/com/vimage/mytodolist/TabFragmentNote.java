package com.vimage.mytodolist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Dimon on 02.01.2017.
 */

public class TabFragmentNote extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.two_tab_fragment_layout, container, false);

        initViews(view);
        //setTextViewClickBehavior();

        return view;

        //return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void initViews(View view) {
//        TextView textViewDescription = (TextView) view.findViewById(R.id.etDescription);
//        textViewDescription.setHint("Enter note");
//
//        TextView textView = (TextView) view.findViewById(R.id.etPerformer);
//        textView.setHint("Enter author name");
//
//        TextView textViewDate = (TextView) view.findViewById(R.id.etDate);
//        textViewDate.setVisibility(View.GONE);// вообще спрячем этот текствью - для заметок он не нужен
    }
}
