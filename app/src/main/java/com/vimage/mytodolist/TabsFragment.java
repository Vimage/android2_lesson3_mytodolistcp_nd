package com.vimage.mytodolist;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import goldzweigapps.tabs.Builder.EasyTabsBuilder;
import goldzweigapps.tabs.Items.TabItem;
import goldzweigapps.tabs.View.EasyTabs;

/**
 * Created by Dimon on 02.01.2017.
 */

public class TabsFragment extends Fragment {
    EasyTabs easyTabs;
    TabFragmentTask firstFragment;
    TabFragmentNote secondFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tabs_fragment_layout, container, false);
        initViews(view);
        setTabs();

        return view;
        //return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void initViews(View view) {
        easyTabs = (EasyTabs)view.findViewById(R.id.easyTabs);

    }

    private void setTabs() {
         firstFragment = new TabFragmentTask();
         secondFragment = new TabFragmentNote();


        EasyTabsBuilder.with(easyTabs).addTabs(
                new TabItem(firstFragment, "Task"),
                new TabItem(secondFragment, "Note")

        ).setTabsBackgroundColor(getResources().getColor(R.color.colorPrimary))
                .setIndicatorColor(getResources().getColor(R.color.md_white_1000)).Build();
    }



}
