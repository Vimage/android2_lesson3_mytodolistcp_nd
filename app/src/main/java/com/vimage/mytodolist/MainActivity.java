package com.vimage.mytodolist;

import android.app.DatePickerDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.vimage.mytodolist.data.TaskContract;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    final Uri TASK_URI = Uri.parse("content://com.vimage.providers.Tasks/tasks");

    RecyclerView recyclerView;
    ArrayList<TaskModel> tasks = new ArrayList<>();
    AdapterTasks tasksAdapter;
    CharSequence[] nChooseList = {"Perform task", "Edit task", "Delete task", "View performer tasks", "Delete all performer tasks"};
    int selectedTaskPosition;
    TaskModel currentTask;

    static final private int ADD_ITEM = 0;
    static final private int EDIT_ITEM = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.rvTasks);
        //fillArrayList();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        tasksAdapter = new AdapterTasks(tasks, MainActivity.this);
        recyclerView.setAdapter(tasksAdapter);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addItemToTaskList();
//                Snackbar.make(view, "Task has been added to list", Snackbar.LENGTH_SHORT)
//                        .setAction("Action", null).show();
            }
        });

        //taskDbHelper = new TaskDBHelper(this);
        createDrawer(toolbar);
        readTasks();

    }

    private void createDrawer(Toolbar toolbar) {
        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withName("Task/notes actions").withIcon(R.mipmap.ic_launcher);
        SecondaryDrawerItem item2 = new SecondaryDrawerItem().withIdentifier(1).withName("Add new item");
        SecondaryDrawerItem item3 = new SecondaryDrawerItem().withIdentifier(2).withName("View All items");

//create the drawer and remember the `Drawer` result object
        Drawer result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .addDrawerItems(
                        item1,
                        new DividerDrawerItem(),
                        item2,
                        new DividerDrawerItem(),
                        item3
                        //new SecondaryDrawerItem().withName("rtgrtyrty")

                )
//                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
//                    @Override
//                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
//                        // do something with the clicked item :D
//                    }
//                }
//                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        int id = (int) drawerItem.getIdentifier();
                        switch (id) {
                            case 1: {
                                addItemToTaskList();
                                //Toast.makeText(MainActivity.this, "Это пункт 1", Toast.LENGTH_SHORT).show();
                                break;
                            }
                            case 2: {
                                clearView();

                                readTasks();
                                Toast.makeText(MainActivity.this, "All tasks view", Toast.LENGTH_SHORT).show();

                                break;
                            }
                        }
                        return false;
                    }
                })
                .build();
    }

    private void readTasks() {
        //чтение из БД
        Cursor cursor2 = getContentResolver().query(TASK_URI, null, null, null, null);

        while (cursor2.moveToNext()) {
            String id = cursor2.getString(cursor2
                    .getColumnIndex(TaskContract.TaskEntry._ID));
            String description = cursor2.getString(cursor2
                    .getColumnIndex(TaskContract.TaskEntry.COLUMN_DESCRIPTION));

            long dueToDate = cursor2.getLong(cursor2
                    .getColumnIndex(TaskContract.TaskEntry.COLUMN_DUE_TO_DATE));

            long performDate = cursor2.getLong(cursor2
                    .getColumnIndex(TaskContract.TaskEntry.COLUMN_PERFORM_DATE));

            String performer = cursor2.getString(cursor2
                    .getColumnIndex(TaskContract.TaskEntry.COLUMN_PERFORMER));

            String itemType = cursor2.getString(cursor2
                    .getColumnIndex(TaskContract.TaskEntry.COLUMN_ITEM_TYPE));

            fillTask(description, performer, dueToDate, performDate, id, itemType);

        }
        cursor2.close();
    }


    public void onTaskListItemClicked(View view) {
        int itemAdapterPosition = recyclerView.getChildAdapterPosition(view);
        if (itemAdapterPosition == RecyclerView.NO_POSITION) {
            return;
        }
        selectedTaskPosition = itemAdapterPosition;
        currentTask = tasks.get(selectedTaskPosition);
        tasksAdapter.setFocused(itemAdapterPosition);

    }

    public void onTaskListItemLongClicked(View view) {
        int itemAdapterPosition = recyclerView.getChildAdapterPosition(view);
        if (itemAdapterPosition == RecyclerView.NO_POSITION) {
            return;
        }
        selectedTaskPosition = itemAdapterPosition;
        currentTask = tasks.get(selectedTaskPosition);
        createListDialog(view);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    private void clearView() {
        tasks.clear(); //clear list
        tasksAdapter.notifyDataSetChanged();
    }

    public void addItemToTaskList() {
        createCustomCodeDialog(null);
    }

    public void createTask(String description, String performer, long dueToDate, String itemType) {
        currentTask = new TaskModel();
        currentTask.description = description;
        currentTask.performer = performer;
        ////currentTask.description = "Новая задача";
        currentTask.dueToDate = dueToDate;
        currentTask.performDate = System.currentTimeMillis();
        currentTask.itemType = itemType;
        tasksAdapter.addItemAtLastPosition(currentTask);
        //createCustomCodeDialog(null);
        insertTaskToDB(currentTask);

    }

    public void fillTask(String description, String performer, long dueToDate, long performDate, String id, String itemType) {
        currentTask = new TaskModel();
        currentTask.description = description;
        currentTask.performer = performer;
        ////currentTask.description = "Новая задача";
        currentTask.dueToDate = dueToDate;
        currentTask.performDate = performDate;
        currentTask.id = id;
        currentTask.itemType = itemType;

        tasksAdapter.addItemAtLastPosition(currentTask);
    }

    private void insertTaskToDB(TaskModel currentTask) {
        //вставка задачи
        ContentValues values = new ContentValues();
        values.put(TaskContract.TaskEntry.COLUMN_DESCRIPTION, currentTask.description);
        values.put(TaskContract.TaskEntry.COLUMN_DUE_TO_DATE, currentTask.dueToDate);
        values.put(TaskContract.TaskEntry.COLUMN_PERFORM_DATE, currentTask.performDate);
        values.put(TaskContract.TaskEntry.COLUMN_PERFORMER, currentTask.performer);

        Uri newUri = getContentResolver().insert(TASK_URI, values);
        Toast.makeText(this, "Задача вставлена: " + newUri.toString(), Toast.LENGTH_SHORT).show();
    }


    private void updateTaskInDB(TaskModel currentTask) {
        //редактирование строки в БД записи
        ContentValues values = new ContentValues();
        values.put(TaskContract.TaskEntry.COLUMN_DESCRIPTION, currentTask.description);
        values.put(TaskContract.TaskEntry.COLUMN_DUE_TO_DATE, currentTask.dueToDate);
        values.put(TaskContract.TaskEntry.COLUMN_PERFORM_DATE, currentTask.performDate);
        values.put(TaskContract.TaskEntry.COLUMN_PERFORMER, currentTask.performer);
        Uri uri = ContentUris.withAppendedId(TASK_URI, Long.parseLong(currentTask.id));
        //int cnt =
        getContentResolver().update(uri, values, null, null);
    }

    private void deleteTaskInDB(TaskModel currentTask) {
        //удаление строки в БД
        Uri uri = ContentUris.withAppendedId(TASK_URI, Long.parseLong(currentTask.id));
        getContentResolver().delete(uri, null, null);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_ITEM) {
            if (resultCode == RESULT_OK) {

                Date date = null;
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                try {
                    date = format.parse(data.getStringExtra(AddNewItem.DUE_TO));

                } catch (ParseException e) {

                    e.printStackTrace();
                }
                createTask(data.getStringExtra(AddNewItem.DESCRIPTION), data.getStringExtra(AddNewItem.PERFORMER), date.getTime()
                        , data.getStringExtra(AddNewItem.ITEM_TYPE));
            }
        }
    }

    public void createCustomCodeDialog(View view) {
//новая версия с вызвовом активити

        Intent intent = new Intent(this, AddNewItem.class);
        startActivityForResult(intent, ADD_ITEM);


//
//        //final View viewForDialog = createViewForDialog(currButton.getText().toString());
//        final View viewForDialog = createViewForDialog("", "");
//        new AlertDialog.Builder(MainActivity.this)
//                .setTitle("Enter task properties")
//                .setView(viewForDialog)
//                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                })
//                .setPositiveButton("Create task", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        Spinner itemType = (Spinner) ((LinearLayout) viewForDialog).getChildAt(0);
//                        String itemTypeText = itemType.getSelectedItem().toString();
//                        EditText editText = (EditText) ((LinearLayout) viewForDialog).getChildAt(1);//(EditText) viewForDialog.findViewById(123);
//                        EditText performerText = (EditText) ((LinearLayout) viewForDialog).getChildAt(2);
//                        EditText dateText = (EditText) ((LinearLayout) viewForDialog).getChildAt(3);
//
//                        //dateText.
//
//
//                        Date date = null;
//                        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
//                        try {
//                            date = format.parse(String.valueOf(dateText.getText()));
//
//                            //System.out.println(date);
//
//                        } catch (ParseException e) {
//
//                            e.printStackTrace();
//                        }
//
//                        createTask(editText.getText().toString(), performerText.getText().toString(), date.getTime(), itemTypeText);
//
//                    }
//                })
//                .setCancelable(false)
//                .show();
    }


    public void createCustomCodeDialogForEdit(View view) {

        //final View viewForDialog = createViewForDialog(currButton.getText().toString());
        final View viewForDialog = createViewForDialog(currentTask.description, currentTask.performer);
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Enter task properties")
                .setView(viewForDialog)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("Apply changes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText editText = (EditText) ((LinearLayout) viewForDialog).getChildAt(0);//(EditText) viewForDialog.findViewById(123);
                        EditText performerText = (EditText) ((LinearLayout) viewForDialog).getChildAt(1);
                        EditText dateText = (EditText) ((LinearLayout) viewForDialog).getChildAt(2);
                        //dateText.


                        Date date = null;
                        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                        try {
                            date = format.parse(String.valueOf(dateText.getText()));

                            //System.out.println(date);

                        } catch (ParseException e) {

                            e.printStackTrace();
                        }

                        editTask(editText.getText().toString(), performerText.getText().toString(), date.getTime());

                    }
                })
                .setCancelable(false)
                .show();
    }

    public void editTask(String description, String performer, long dueToDate) {
        //currentTask = new TaskModel();
        currentTask.description = description;
        currentTask.performer = performer;
        currentTask.dueToDate = dueToDate;
        currentTask.performDate = System.currentTimeMillis();
        tasksAdapter.changeItemAtPosition(selectedTaskPosition);
        updateTaskInDB(currentTask);

    }


    private View createViewForDialog(String editingText, String performer) {
        LinearLayout linearLayout = new LinearLayout(MainActivity.this);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams layoutParamsForView = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParamsForView.gravity = Gravity.CENTER_HORIZONTAL;


        Spinner spItemType = new Spinner(MainActivity.this);
        ArrayAdapter<String> namesAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.items_list));

        namesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spItemType.setAdapter(namesAdapter);


//        EditText editTextItemtype = new EditText(MainActivity.this);
//        if (editingText.equals("")) {
//            editTextItemtype.setHint("Enter Item Type");
//        } else {
//            editTextItemtype.setText(editingText);
//        }
//        editTextItemtype.setTextSize(20);

        linearLayout.addView(spItemType, layoutParamsForView);


        EditText editText = new EditText(MainActivity.this);
        if (editingText.equals("")) {
            editText.setHint("Enter task description");
        } else {
            editText.setText(editingText);
        }
        editText.setTextSize(20);
        linearLayout.addView(editText, layoutParamsForView);

        EditText editTextPerformer = new EditText(MainActivity.this);
        if (performer.equals("")) {
            editTextPerformer.setHint("Enter performer name");
        } else {
            editTextPerformer.setText(performer);
        }
        editTextPerformer.setTextSize(20);
        linearLayout.addView(editTextPerformer, layoutParamsForView);


// получим текущую дату
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        final EditText editDate = new EditText(MainActivity.this);
        editDate.setText(day + "-" + (month) + "-" + year);
        editDate.setInputType(InputType.TYPE_NULL);
        //editDate.setInputType(InputType.TYPE_DATETIME_VARIATION_DATE);
        editDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDatePickerDialog(null, editDate);


            }
        });

        linearLayout.addView(editDate, layoutParamsForView);


        return linearLayout;
    }

    DialogInterface.OnClickListener dialogIntfListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            switch (i) {
                case 0:
                    tasksAdapter.setPerormed(selectedTaskPosition);
                    //updateTaskInDB(currentTask);
                    //createCustomCodeDialog("fggggggggggggg");
                    //Toast.makeText(MainActivity.this, "0", Toast.LENGTH_SHORT).show();
                    break;
                case 1:
                    createCustomCodeDialogForEdit(null);
                    //createCustomCodeDialog(null);
                    //deleteButton(currButton);
                    break;

                case 2:
                    deleteTaskInDB(currentTask); // удаляем выбранную задачу
                    tasksAdapter.removeItemAtPosition(selectedTaskPosition);
                    ////deleteButton(currButton);
                    break;
                case 3: // выборка всех задач по исполнителю
                    clearView();
                    selectAllPerformerTasksInDB(currentTask);
                    break;
                case 4: //удаление всех задач по исполнителю

                    deleteAllPerformerTasks(currentTask);
                    break;
                default:


                    //Toast.makeText(MainActivity.this, nChooseList[i], Toast.LENGTH_SHORT).show();
            }
        }
    };

    private boolean deleteAllPerformerTasks(final TaskModel task) {

        AlertDialog.Builder ad;
        ad = new AlertDialog.Builder(MainActivity.this);
        ad.setTitle("All performer tasks delete");  // заголовок
        ad.setMessage("All " + task.performer + "'s tasks will be deleted.\nPlease, confirm."); // сообщение
        ad.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        try {
                            deleteAllPerformerTasksInDB(task);
                        } finally {
                            clearView();
                            readTasks();
                            Toast.makeText(MainActivity.this, "All " + task.performer + "'s tasks deleted.",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }
        );

        ad.setNegativeButton("NO", new DialogInterface.OnClickListener()

                {
                    public void onClick(DialogInterface dialog, int arg1) {

                    }
                }

        );
        ad.setCancelable(true);
        ad.show();
        return false;
    }

    private void deleteAllPerformerTasksInDB(TaskModel task) {
        //удаление из БД
//        String query = "DELETE FROM " + TaskContract.TaskEntry.TABLE_NAME +
//                " WHERE performer = '" + task.performer + "'";

        getContentResolver().delete(TASK_URI, "performer=?", new String[]{task.performer});
    }

    private void selectAllPerformerTasksInDB(TaskModel task) {
        //выборка из БД. задачи по исполнителю
        Cursor cursor2 = getContentResolver().query(TASK_URI, null, "performer=?", new String[]{task.performer}, null);
        while (cursor2.moveToNext()) {
            String id = cursor2.getString(cursor2
                    .getColumnIndex(TaskContract.TaskEntry._ID));
            String description = cursor2.getString(cursor2
                    .getColumnIndex(TaskContract.TaskEntry.COLUMN_DESCRIPTION));

            long dueToDate = cursor2.getLong(cursor2
                    .getColumnIndex(TaskContract.TaskEntry.COLUMN_DUE_TO_DATE));

            long performDate = cursor2.getLong(cursor2
                    .getColumnIndex(TaskContract.TaskEntry.COLUMN_PERFORM_DATE));

            String performer = cursor2.getString(cursor2
                    .getColumnIndex(TaskContract.TaskEntry.COLUMN_PERFORMER));

            String itemType = cursor2.getString(cursor2
                    .getColumnIndex(TaskContract.TaskEntry.COLUMN_ITEM_TYPE));


            fillTask(description, performer, dueToDate, performDate, id, itemType);

        }
        cursor2.close();

    }

    public void createListDialog(View view) {


        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Choose action")
                .setItems(nChooseList, dialogIntfListener)
                .setNeutralButton("Cancel", null)
                .show();

    }

    public void createDatePickerDialog(View view, final EditText editDate) {

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        //String selDate = "";

        new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                //Toast.makeText(MainActivity.this, dayOfMonth + "-" +monthOfYear + "-" + year, Toast.LENGTH_SHORT).show();
                editDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
            }
        }, year, month, day).
                show();


    }


}
