package com.vimage.mytodolist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import java.text.SimpleDateFormat;

import goldzweigapps.tabs.View.EasyTabs;

public class AddNewItem extends AppCompatActivity {
    public final static String DESCRIPTION = "com.vimage.mytodolist.DESCRIPTION";
    public final static String PERFORMER = "com.vimage.mytodolist.PERFORMER";
    public final static String DUE_TO = "com.vimage.mytodolist.DUE_TO";
    public final static String ITEM_TYPE = "com.vimage.mytodolist.ITEM_TYPE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_new_item);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Add new item");

    }


    public void onApplyClick(View v) {
        String descriptionText;
        String performerText;
        String dateText;
        String itemType;


        descriptionText="";
        performerText="";
        itemType="TASK";

        long date = System.currentTimeMillis();
        SimpleDateFormat sdf =  new SimpleDateFormat("dd-MM-yyyy");;
        dateText = sdf.format(date);

        Intent answerIntent = new Intent();

        EasyTabs easyTabs = (EasyTabs) findViewById(R.id.easyTabs);

        switch  (easyTabs.getTabLayout().getSelectedTabPosition()){
            case 0:{
                 descriptionText = ((EditText)  findViewById(R.id.etDescription)).getText().toString();
                 performerText = ((EditText)  findViewById(R.id.etPerformer)).getText().toString();
                 dateText = ((EditText)  findViewById(R.id.etDate)).getText().toString();

                break;
            }
            case 1:{
                descriptionText = ((EditText)  findViewById(R.id.etNote)).getText().toString();
                performerText = ((EditText)  findViewById(R.id.etAuthor)).getText().toString();
                itemType="NOTE";

                break;
            }

        }


        answerIntent.putExtra(DESCRIPTION, descriptionText);
        answerIntent.putExtra(PERFORMER, performerText);
        answerIntent.putExtra(DUE_TO, dateText);
        answerIntent.putExtra(ITEM_TYPE, itemType);

        setResult(RESULT_OK, answerIntent);
        finish();
    }
}
